const mongoose = require('mongoose');
const URL = "mongodb+srv://admin:admin@cluster0.uxz8l.mongodb.net/school?retryWrites=true&w=majority";
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
const connectDB = async() => {
    await mongoose.connect(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    console.log("DB CONNECT");
}
module.exports = connectDB;
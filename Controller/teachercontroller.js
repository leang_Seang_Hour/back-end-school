const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

const teacher = require('../Model/teachermodel');
//CREATE DATA
router.post('/', async(req, res) => {
    let tea = new teacher({
        tname: req.body.tname,
        ttelephone: req.body.ttelephone,
    })
    try {
        let Modelteacher = await tea.save();
        res.json(tea);
        console.log(Modelteacher);
    } catch (err) {
        console.log('error' + JSON.stringify(err, undefined, 2));
    }
});
//View DATA
router.get('/', async(req, res) => {
        teacher.find((err, doc) => {
            if (!err) {
                res.send(doc);
            } else {
                console.log('Error' + JSON.stringify(err, undefined, 2));
            }
        })
    })
    //Update Data
router.put('/:id', (req, res) => {
        if (!ObjectId.isValid) {
            return res.status(400).send(`Error No data for Update in ID ${req.params.id}`)
        }
        let tea = {
            tname: req.body.tname,
            ttelephone: req.body.ttelephone
        }
        teacher.findByIdAndUpdate(req.params.id, { $set: tea }, { new: true }, (err, doc) => {
            if (!err) {
                res.send(doc);
            } else {
                console.log('Error' + JSON.stringify(err, undefined, 2));
            }
        })
    })
    //Delete 
router.delete('/:id', (req, res) => {
        if (!ObjectId.isValid) {
            return res.status(400).send(`No Data for DELETE ID: ${req.params.id}`);
        }
        teacher.findByIdAndDelete(req.params.id, (err, doc) => {
            if (!err) {
                res.send(doc);
            } else {
                console.log('Error ' + JSON.stringify(err, undefined, 2));
            }
        })
    })
    //GET 1 DATA 
router.get('/:id', (req, res) => {
    if (!ObjectId.isValid) {
        return res.status(400).send(`No Data for View `);
    }
    teacher.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('Error ' + JSON.stringify(err, undefined, 2));
        }
    })
})
module.exports = router;
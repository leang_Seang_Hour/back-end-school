const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;

const student = require('../Model/studentmodel');
//Create DATA
router.post('/', async(req, res) => {
    let stu = new student({
        sfirstname: req.body.sfirstname,
        slastname: req.body.slastname,
        stelephone: req.body.stelephone,
        sgender: req.body.sgender,
        saddress: req.body.saddress
    });
    try {
        let modelStudent = await stu.save();
        res.json(modelStudent);
        console.log(modelStudent);
    } catch (err) {
        res.send(err);
        console.log(err);
    }
});
//View all Data
router.get('/', (req, res) => {
    student.find((err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log('error' + JSON.stringify(err, undefined, 2));
        }
    });
});
//Get 1 Data 
router.get('/:id', (req, res) => {
    if (!ObjectId.isValid(req.params.id))
        return res.status(400).send(`No data in DB ${req.params.id}`);
    student.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log("Error Get Person DATA");
        }
    });
});
//DELETE 1 DATA
router.delete('/:id', (req, res) => {
    if (!ObjectId.isValid) {
        return res.status(400).send(`No Data in DB ${req.params.id}`);
    }
    student.findByIdAndDelete(req.params.id, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log("Error in delete Data " + JSON.stringify(err, undefined, 2));
        }
    })
});
//Update 1 DATA
router.put('/:id', async(req, res) => {
    if (!ObjectId.isValid) {
        return res.status(400).send(`No Found Data`);
    }
    let stu = {
        sfirstname: req.body.sfirstname,
        slastname: req.body.slastname,
        stelephone: req.body.stelephone,
        sgender: req.body.sgender,
        saddress: req.body.saddress,
    };
    console.log(stu);
    await student.findByIdAndUpdate(req.params.id, { $set: stu }, { new: true }, (err, doc) => {
        if (!err) {
            res.send(doc);
        } else {
            console.log("Error Update Data" + JSON.stringify(err, undefined, 2));
        }
    })
})

module.exports = router;
const mongoose = require('mongoose');

var student = new mongoose.Schema({
    sid: { type: String },
    sfirstname: { type: String },
    slastname: { type: String },
    stelephone: { type: String },
    sgender: { type: String },
    saddress: { type: String }
});
//'student' is table name 
module.exports = student = mongoose.model('student', student);
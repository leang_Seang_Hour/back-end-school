const moongose = require('mongoose');

var teacher = new moongose.Schema({
    tname: { type: String },
    ttelephone: { type: String }
});
module.exports = teacher = moongose.model('Teacher', teacher);
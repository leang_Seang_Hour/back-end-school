const express = require('express');
const stucontroller = require('./Controller/studentcotroller.js');
const teacontroller = require('./Controller/teachercontroller.js');
const connectDB = require('./Database/ConnectDB.js')
const mongoose = require('mongoose');
const app = express();
const bodyparser = require('body-parser');
const cors = require('cors');
connectDB();
mongoose.Promise = global.Promise;

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({ limit: '10mb', extended: true }));
app.use(bodyparser.urlencoded({ extended: true }));

app.use(cors({ origin: 'http://localhost:4200' }));
app.use('/teacher', require('./Controller/teachercontroller'));
app.use('/student', require('./Controller/studentcotroller'));

app.listen(3000, () => console.log("port Start at 3000"));